# co_craw

#### 介绍
使用swoole协程mysql+redis连接池 使用爬虫采集 掘金 开发者头条 博客园的一些文章到自己的数据库方便 每天自己的阅读 



#### 安装教程

1. php7+swoole4.0
2. querylist


#### 使用说明

1. PHP start.php
2. 定时任务执行  crontab_xx.sh的2个文件
3. 按照 article下的文件 创建数据库

我的网站
http://craw.cibn.top/

声明 

此程序完全为第三者开发者开发，仅用于学习交流，禁止用于其他用途!